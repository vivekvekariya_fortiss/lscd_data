# Artifacts and Dataset for submission to ICSE-2024

This is part of submission of the paper titled - "Enhancing Test Dataset Quality for Deep Neural Networks with Latent Space Class Dispersion (LSCD" at ICSE-2024.

## Getting started

You have downloaded some sample corner case dataset (the imprvoment set) generated using Coverage-Guided Fuzzing for LSCD maximization. 

## Various Folders

1) crashes_rec (Images with recall <= 0.5)
2) crashes_f1 (Images with f1_score <= 0.5)
3) crashes_hyb (Images with hybrid function <= 0.5)

These folders contain various corner case images identified as per various objective functions described in Sec. 3.3 (Corner Case Identification).

## Files

Each folder has various corner cases with their corresponding .json file that contains metadata associated with the corner cases.

Exa. id_014768_20181204135952_camera_frontcenter_000107105 
--> id_014768 is ID assigned to corner case image during CGF.
--> Original image name: 20181204135952_camera_frontcenter_000107105 in A2D2 test dataset.

1) .png file: It is identified 300x300 corner case image from the original image. 
2) .json file: It has various meta-data corresponding to it. 
    
        a) "__image_quality_metrics__": Describes transformations applied to the original image, SSIM & MSE values. Overall results are discussed in RQ3.
        b) "__other_metrics__": Descibes evalution results i.e. number of True Positives (TPs), False Negatives (FNs), False Positives (FPs), Precision, Recall, f1_score, etc.
        c) "__mtrc010xx__": Describes image-wise evaluation scores for the mertrics mean IOU, mAP, mean Confidence (on TPs).
        d) "__gt_labels__": Has Ground truth coordinates and classes of the input image.
